# atlas-case-study

> Atlas Muse, WP decoulded test

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
$ cd directory
$ pm2 start npm -i 2 --name "atlas-case-study" -- start
$ cluster mode: pm2 start npm -i 2 --name "atlas-case-study" -- start

# pm2 operations
$ cd directory
$ pm2 stop ID / all
$ pm2 delete ID / all
```
For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
