const pkg = require('./package')
const webpack = require('webpack')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'robots', name: 'robots', content: 'noindex,follow' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/what-input/5.1.1/what-input.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.5.0-rc.2/js/foundation.min.js'},
      { src: 'https://unpkg.com/vue-masonry@0.11.3/dist/vue-masonry-plugin-window.js'}
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { 
    color: '#000000',
    height: '5px' 
  },

  /*
  ** Global CSS
  */
 css: [
    '~/assets/scss/main.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.BASE_URL || 'https://wp-rest.fancyapps.blog/wp-json/',
    credentials: false,
    proxy: true,
    // prefix: '/api/'
  },
  proxy: {
    '/stay-in-touch/': { target: 'https://wp-rest.fancyapps.blog/gravityformsapi/forms/3/submissions?api_key=18897db720', pathRewrite: {'^/stay-in-touch/': ''}, changeOrigin: true },
    '/contact-us/': { target: 'https://wp-rest.fancyapps.blog/gravityformsapi/forms/2/submissions?api_key=18897db720', pathRewrite: {'^/contact-us/': ''}, changeOrigin: true },
    '/keep-in-touch/': { target: 'https://wp-rest.fancyapps.blog/gravityformsapi/forms/1/submissions?api_key=18897db720', pathRewrite: {'^/keep-in-touch/': ''}, changeOrigin: true },
    '/fancy-squares-core/': { target: 'https://wp-rest.fancyapps.blog/wp-admin/admin-ajax.php', pathRewrite: {'^/fancy-squares-core/': ''}, changeOrigin: true },
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
   vendor: [
     'jquery'
  ],
   plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
