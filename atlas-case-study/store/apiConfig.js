"use strict"

const API_CONFIG = {
    baseUrl: 'https://wp-rest.fancyapps.blog/wp-json/',
    basePostsUrl: 'wp/v2/posts',
    basePagesUrl: 'wp/v2/pages',
    api: {
        popularPosts: "https://wp-rest.fancyapps.blog/wp-json/wp/v2/posts?per_page=3&filter[suppress_filters]=false&filter[orderby]=post_views"
    }
}

module.exports = API_CONFIG;