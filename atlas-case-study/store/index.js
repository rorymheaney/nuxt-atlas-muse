import Vuex from 'vuex'
import API_CONFIG from './apiConfig.js'
import mutations from './mutations'
import actions from './actions'

const createStore = () => {
  return new Vuex.Store({
    state: {
      mainClass: '',
      pageTemplate: '',
      about_page: [],
      cat_nyc: [],
      cat_wander: [],
      home_top_three: [],
      home_first_nine: [],
      shop_styles: [],
      popularPosts: []
    },
    mutations,
    actions
  })
}

export default createStore

// console.log(API_CONFIG.api);