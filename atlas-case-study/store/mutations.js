const mutations = {
    mainClassChange(state) {
        state.mainClass
    },
    pageTemplateChange(state) {
        state.pageTemplate
    },
    setPopularPosts (state, obj) {
        state.popularPosts = obj
    }
}
  
export default mutations