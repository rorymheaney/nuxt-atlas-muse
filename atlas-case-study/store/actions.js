import mutations from './mutations'
import API_CONFIG from './apiConfig.js'

let arr = []
let count = 0

const actions = {
    async nuxtServerInit ({ commit }, { app }) {
        // console.log(API_CONFIG.api.popularPosts);
        /**
         * This is the secret sauce.
         * If the data being requested is cached, subsequent API calls will not be made
         * Also, if using nuxt generate, nuxtServerInit will be called for every page
         * Because of this caching, the API calls will only be done once
         */
        if (count === 0 || count > 15) {
            count = 0
            arr = []
            // console.log('============= Server Init API calls =============')
            try {
                // console.log('home')
                let PP = await app.$axios.get(
                    API_CONFIG.api.popularPosts
                )
                // console.log(PP.data);
                arr.push(PP.data)
                commit('setPopularPosts', PP.data)

                // console.log('case studies')
                // let projects = await app.$axios.get(
                // API_CONFIG.wpDomain + API_CONFIG.api.projects
                // )
                // arr.push(projects.data)
                // commit('setProjects', projects.data)
                count++
            } catch (e) {
                console.log('error with API')
                arr = []
            }
        } else {
            count++
            // console.log('using cached api pew')
            commit('setPopularPosts', arr[0])
            // console.log(arr[0]);
            // commit('setProjects', arr[1])
        }
    }
}

export default actions